const headers = [
  'Contact Name',
  'Contact Phone',
  'Company',
  'Unit',
  'Park',
  'Floor',
  'Description',
  'Assigned To',
];

export default headers;
