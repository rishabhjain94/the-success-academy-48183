import React, { useEffect } from 'react';
import './App.css';
import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import Import from './components/Import';

const App = () => {
  useEffect(() => {
    M.AutoInit();
  });

  return <Import />;
};

export default App;
