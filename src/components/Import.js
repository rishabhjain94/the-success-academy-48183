import React, { useState } from 'react';
import { ExcelRenderer } from 'react-excel-renderer';
import headers from '../headers';
import M from 'materialize-css/dist/js/materialize.min.js';
import { Document, BlobProvider } from '@react-pdf/renderer';
import GetResult from './GetResult';
import Spinner from './Spinner';
import DownloadFile from './DownloadFile';
const Import = () => {
  const [imported, loadExcel] = useState(null);
  const [assignee, loadAssignee] = useState([]);
  const [show, setShow] = useState(false);
  const fileHandler = async (event) => {
    loadExcel(null);
    loadAssignee([]);
    let fileObj = event.target.files[0];
    let assignees = [];
    let data = {};
    setShow(true);
    try {
      await ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
          console.log(err);
        } else {
          if (JSON.stringify(resp.rows[1]) === JSON.stringify(headers)) {
            console.log(resp);
            resp.rows.slice(2).forEach((item) => {
              let assignee = item[7];
              if (
                Object.keys(data).length !== 0 &&
                data.hasOwnProperty(assignee)
              ) {
                data[assignee].push(item);
              } else {
                data[assignee] = [item];
              }
              if (!assignees.includes(item[7])) {
                assignees.push(item[7]);
              }
            });
            console.log(assignees);
            console.log(data);
            loadExcel(data);
            loadAssignee(assignees);
          } else {
            M.toast({ html: `Error: File format doesn't match!` });
          }
        }
      });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div>
      <div
        className='file-field input-field container'
        style={{ width: '70%', height: '30vh' }}
      >
        <div style={{ position: 'relative', top: '12vh' }}>
          <div
            className='btn-small waves-effect waves-light'
            style={{ backgroundColor: '#47a7ff' }}
          >
            <i className='material-icons left'>cloud_upload</i>
            Upload
            <input type='file' onChange={fileHandler} accept='.xlsx' />
          </div>
          <div className='file-path-wrapper'>
            <input className='file-path validate' type='text' />
          </div>
        </div>
      </div>
      {show && imported && (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            position: 'relative',
          }}
        >
          <BlobProvider
            className='btn-large waves-effect waves-light '
            document={
              <Document>
                {assignee.map((item, pos) => (
                  <GetResult
                    imported={imported}
                    assignee={item}
                    key={pos}
                    pos={pos}
                    total={assignee.length}
                  />
                ))}
              </Document>
            }
          >
            {({ blob, url, loading, error }) => {
              return loading ? (
                <Spinner />
              ) : (
                <DownloadFile urlIs={url} blob={blob.size} />
              );
            }}
          </BlobProvider>
        </div>
      )}
    </div>
  );
};
export default Import;
