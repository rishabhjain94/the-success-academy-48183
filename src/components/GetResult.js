import React, { useState } from 'react';
import {
  PDFDownloadLink,
  Page,
  Text,
  View,
  Document,
  StyleSheet,
} from '@react-pdf/renderer';
const date = new Date().toDateString();
const styles = StyleSheet.create({
  page: {
    backgroundColor: '#ffffff',
    paddingTop: 15,
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 15,
  },
  section: { color: 'white', textAlign: 'center', margin: 30 },
  pageNumbers: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    textAlign: 'center',
  },
  taskOptions: {
    border: '1px solid #78909c',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
    paddingTop: 6,
    paddingRight: 6,
    paddingBottom: 6,
    paddingLeft: 6,
    marginBottom: 18,
  },
  taskOption: {
    marginBottom: 7.5,
  },
  taskNumber: {
    marginBottom: 9,
    textAlign: 'center',
    color: '#757575',
    fontWeight: 'bold',
    fontSize: 15,
  },
  assignee: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 21,
    color: '#409AA0 ',
  },
  taskContent: {
    fontSize: 11,
    color: '#616161',
    fontWeight: 400,
    marginLeft: 9,
  },
  twoInARow: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  pageNumbers: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: '#9e9e9e',
    fontSize: 6,
  },
});
const GetResult = ({ imported, assignee, pos, total }) => {
  return (
    <Page size='A4' style={styles.page}>
      <Text fixed style={styles.pageNumbers}>{`Assignee # ${
        pos + 1
      } / ${total}`}</Text>
      <Text
        style={{
          fontSize: 6,
          color: '#9e9e9e',
          textAlign: 'right',
          marginBottom: 17,
        }}
        fixed
      >{`The Success Academy || ${assignee} || ${date}`}</Text>

      <View>
        <Text style={styles.assignee}>Maintenance Requests - {assignee}</Text>
        {imported[assignee].map((items, posi) => (
          <View key={posi} wrap={false}>
            <Text style={styles.taskNumber}>Request #{posi + 1}</Text>
            <View style={styles.taskOptions}>
              <View>
                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Contact Name:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[0]}</Text>
                </Text>

                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Contact Phone:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[1]}</Text>
                </Text>
              </View>
              <View>
                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Company:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[2]}</Text>
                </Text>

                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Unit:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[3]}</Text>
                </Text>
              </View>
              <View>
                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Park:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[4]}</Text>
                </Text>

                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Floor:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[5]}</Text>
                </Text>
              </View>
              <Text style={styles.taskOption}>
                <span
                  style={{
                    fontSize: 13,
                    color: '#424242',
                    fontWeight: 'bold',
                    letterSpacing: 1,
                  }}
                >
                  Description:{' '}
                </span>
                <Text style={styles.taskContent}>{items[6]}</Text>
              </Text>
              <View style={styles.twoInARow}>
                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Assigned To:{' '}
                  </span>
                  <Text style={styles.taskContent}>{items[7]}</Text>
                </Text>

                <Text style={styles.taskOption}>
                  <span
                    style={{
                      fontSize: 13,
                      color: '#424242',
                      fontWeight: 'bold',
                      letterSpacing: 1,
                    }}
                  >
                    Signature:{' '}
                  </span>
                  <Text
                    style={styles.taskContent}
                  >{`_____________________________`}</Text>
                </Text>
              </View>
            </View>
          </View>
        ))}
      </View>
    </Page>
  );
};
export default GetResult;
