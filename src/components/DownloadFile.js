import React from 'react';

const DownloadFile = ({ urlIs, blob }) => {
  const date = new Date().toLocaleDateString('en-za', {
    month: 'short',
    day: '2-digit',
  });
  const fileName = `Maintenance-Requests-${date}.pdf`;
  console.log('enter', urlIs, blob);
  if (blob !== 449) {
    var a = window.document.createElement('a');
    a.href = urlIs;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <p className='flow-text center'>
        <span>
          Download didn't trigger,
          <a
            download={fileName}
            href={urlIs}
            className='btn-small waves-effect waves-light grey-text text-darken-3'
            style={{
              marginLeft: '6px',
              marginRight: '6px',
              backgroundColor: '#eeeeee',
              position: 'relative',
              bottom: '3px',
            }}
          >
            <i className='material-icons right'>cloud_download</i>
            Click Here
          </a>
        </span>
        to download manually.
      </p>
    </div>
  );
};

export default DownloadFile;
